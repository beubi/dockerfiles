#!/bin/sh
#########################################################################################
# This file allows you to install docker-compose in a single line
# To use it just run:
#  'wget -qO- https://bitbucket.org/beubi/dockerfiles/raw/master/install.sh | sudo sh'
# Prepared and tested for Ubuntu 14.04
#########################################################################################

echo "=> Preparing docker apt repo"
###################################
apt-get -qq update
apt-get -qq install apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D > /dev/null
echo 'deb https://apt.dockerproject.org/repo ubuntu-trusty main' > /etc/apt/sources.list.d/docker.list
apt-get -qq update

echo "=> Installing dependencies"
#################################
apt-get -qq install linux-image-extra-$(uname -r) -y
apt-get -qq install apparmor -y

echo "=> Installing and starting docker engine"
###############################################
apt-get -qq install docker-engine -y

echo "=> Adding all 'real' users to the docker group (those who use bash shell)"
################################################################################
potential_docker_users=$(getent passwd | grep /bin/bash | cut -d: -f1)
for d_user in $potential_docker_users ; do
  usermod -aG docker $d_user
done

echo "=> Installing docker compose"
###################################
curl -s -L https://github.com/docker/compose/releases/download/1.7.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "=> Installing docker compose bash completion"
###################################################
curl -s -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose

###################################################
echo "=> All done! Test with 'sudo docker info'"